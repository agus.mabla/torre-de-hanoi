tablero = [[4,3,2,1],[],[]]
cant_movimientos = 0
def tercerTorre(torre_v,torre_n):
    return 3-(torre_n+torre_v) #Devuelvo la posicion de la torre faltante restando a la suma de torres las torres que conozco

def mover(torre_v,pos,torre_n):
    global cant_movimientos
    try:
        if len(tablero[torre_v]) < pos:
            raise ValueError("Posicion outOfBounds")
        if len(tablero[torre_n]) > 0 and tablero[torre_v][pos] > tablero[torre_n][-1]:
            raise ValueError("No se puede apilar una pieza grande sobre una chica")
        if len(tablero[torre_v])-1 == pos:
            tablero[torre_n].append(tablero[torre_v][pos])
            tablero[torre_v].pop(-1)
            cant_movimientos += 1
            print(tablero)
            return len(tablero[torre_n])-1 #Devuelvo la posicion en la que quedo la pieza en la nueva torre
        else:
            pos_nueva = mover(torre_v,pos+1,tercerTorre(torre_v,torre_n))
            tablero[torre_n].append(tablero[torre_v][pos])
            tablero[torre_v].pop(-1)
            cant_movimientos += 1
            print(tablero)
            pos_nueva_2 = len(tablero[torre_n])-1
            mover(tercerTorre(torre_v,torre_n),pos_nueva,torre_n)
            return pos_nueva_2

    except Exception as e:
        print(e)

def main():
    mover(0,0,1)
    print(cant_movimientos)
if __name__ == "__main__":
    main()